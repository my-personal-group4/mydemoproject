package tutsproject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import io.github.bonigarcia.wdm.WebDriverManager;

public class anothertryreport 
{
	public WebDriver driver;
	public String testURL = "file:///D:/gotrip/index.html";
	public String thisbrowser = "Chrome";
	
	public ExtentHtmlReporter reportHtml;
	public ExtentReports extent;
	public ExtentTest extenttest;
	
	
	
	public void testResultreport() throws FileNotFoundException
	{
		String ddate = takescreenshots.settingDate();
		reportHtml = new ExtentHtmlReporter("./Reports/testreport_"+ddate+".html");
		reportHtml.config().setEncoding("utf-8");
		reportHtml.config().setDocumentTitle("Automation Test Report ");
		reportHtml.config().setReportName("Automation Tests ");
		reportHtml.config().setTheme(Theme.DARK);
		
		extent = new ExtentReports();
		extent.setSystemInfo("Organisation", "Freelancer");
		extent.setSystemInfo("Browser", "multi-browser");
		extent.attachReporter(reportHtml);
	}
	
	@BeforeTest
	public void beforetest() throws FileNotFoundException
	{
		
		testResultreport();
		selectbrowser(thisbrowser);
	}
	
	@AfterTest
	public void aftertest()
	{
		driver.quit();
		extent.flush();
		
	}
	
	public void successlog()
	{
		String successText = "<b>Test Method Is Successful</b>";
		Markup m = MarkupHelper.createLabel(successText, ExtentColor.GREEN);
		extenttest.log(Status.PASS, m);
	}
	
	public void faillog(String reason)
	{
		String logText = "<b>"+reason+"</b> \n";
		Markup m = MarkupHelper.createLabel(logText, ExtentColor.RED);
		extenttest.log(Status.FAIL, m);
	}
	
	
	
	public void selectbrowser(String browser)
	{
		switch (browser) {
		case "Chrome":
			WebDriverManager.chromedriver().setup();

			// headless
			ChromeOptions chromeoption = new ChromeOptions();
			chromeoption.addArguments("window-size=1920,1080");
			chromeoption.setHeadless(true);

			driver = new ChromeDriver(chromeoption);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// driver.manage().window().maximize();
			driver.get(testURL);
			break;
		case "Firefox":
			WebDriverManager.firefoxdriver().setup();

			// headless
			FirefoxOptions foxoption = new FirefoxOptions();
			foxoption.addArguments("window-size=19200,1080");
			foxoption.setHeadless(true);

			driver = new FirefoxDriver(foxoption);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// driver.manage().window().maximize();
			driver.get(testURL);
			break;
		case "Edge":
			WebDriverManager.edgedriver().setup();

			// headless
			EdgeOptions edgeoption = new EdgeOptions();
			edgeoption.addArguments("window-size=19200,1080");
			edgeoption.setHeadless(true);

			driver = new EdgeDriver(edgeoption);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// driver.manage().window().maximize();
			driver.get(testURL);
			break;
		default:
			System.out.println("ONLY THREE BROWSERS ARE AVAILABLE. PICK ONE ");
		}
	}
	
	
	@Test(priority=1)
	public void TestcaseOne() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseOne");
		System.out.println(" TestcaseOne ");
		String actualURL = driver.getCurrentUrl();
		String eee = "D://gogo.html";
		Thread.sleep(3000);
		
		if(actualURL.equalsIgnoreCase(eee))
		{
			successlog();
		}
		else
		{
			faillog("URL IS NOT MATCH - EXPECTED TO BE "+eee+" BUT SHOWED "+actualURL+" INSTEAD");
			alltestcases.testcaseone(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseOne");
			Assert.fail(" URL NOT MATCH");
		}
	}
	
	@Test(priority=2)
	public void TestcaseTwo() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseTwo");
		System.out.println(" TestcaseTwo ");
		String actualURL = driver.getCurrentUrl();
		Thread.sleep(3000);
		if(actualURL.equalsIgnoreCase(testURL))
		{
			successlog();
		}
		else
		{
			faillog("URL IS NOT MATCH - EXPECTED TO BE "+testURL+" BUT SHOWED "+actualURL+" INSTEAD");
			alltestcases.testcasetwo(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseTwo");
			Assert.fail(" URL NOT MATCH");
		}
	}
	
	@Test(priority=3)
	public void TestcaseThree() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseThree");
		System.out.println(" TestcaseThree ");
		WebElement packagemenu = driver.findElement(By.xpath("//ul[@id=\"navigation\"]//li/a[contains(text(),'Package')]"));
		
		Thread.sleep(3000);
		if(packagemenu.getText().equalsIgnoreCase("PACKAGE"))
		{
			successlog();
		}
		else
		{
			faillog("EXPECTED TO BE \"PACKAGE\" BUT SHOWED "+packagemenu.getText()+" INSTEAD");
			alltestcases.testcasethree(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseThree");
			Assert.fail(" PAACKAGE MENU IS NOT VISIBLE IN THE WEB ");
		}
	}
	
	@Test(priority=4)
	public void TestcaseFour() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseFour");
		System.out.println(" TestcaseFour ");
		WebElement homemenu = driver.findElement(By.xpath("//ul[@id='navigation']//li/a[contains(text(),'Home')]"));
		
		Thread.sleep(3000);
		if(homemenu.getText().equalsIgnoreCase("HOME"))
		{
			successlog();
		}
		else
		{
			faillog("EXPECTED TO BE \"HOME\" BUT SHOWED "+homemenu.getText()+" INSTEAD");
			alltestcases.testcasefour(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseFour");
			Assert.fail(" BLOG MENU IS NOT VISIBLE IN THE WEB ");
		}
	}
	
	@Test(priority=5)
	public void TestcaseFive() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseFive");
		System.out.println(" TestcaseFive ");
		WebElement pagesmenu = driver.findElement(By.xpath("//ul[@id=\"navigation\"]//li/a[contains(text(),'Pages')]"));
		
		Thread.sleep(3000);
		
		if(pagesmenu.getText().equalsIgnoreCase("blog"))
		{
			successlog();
		}
		else
		{
			faillog("EXPECTED TO BE \"BLOG\" BUT SHOWED "+pagesmenu.getText()+" INSTEAD");
			alltestcases.testcasefive(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseFive");
			Assert.fail(" PAGE MENU IS NOT VISIBLE IN THE WEB ");
		}
	}
	

	@Test(priority=6)
	public void TestcaseSix() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseSix");
		System.out.println(" TestcaseSix ");
		WebElement searchbutton = driver.findElement(By.xpath("//form[@class=\"search-box\"]/div/a[contains(text(),'Search')]"));
		
		Thread.sleep(3000);
		if(searchbutton.isEnabled())
		{
			successlog();
		}
		else
		{
			faillog("SEARCH BUTTON IS NOT ACTIVE IN THE WEB");
			alltestcases.testcasesix(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseSix");
			Assert.fail(" SEARCH BUTTON IS NOT ACTIVE IN THE WEB ");
		}
	}
	
	@Test(priority=7)
	public void TestcaseSeven() throws InterruptedException, IOException
	{
		extenttest = extent.createTest("TestcaseSeven");
		System.out.println(" TestcaseSeven ");
		WebElement searchbutton = driver.findElement(By.xpath("//ul[@id='navigation']//li/a[contains(text(),'Contact Us')]"));
		String how = searchbutton.getCssValue("text-transform");
		Thread.sleep(1000);
		
		if(searchbutton.isEnabled())
		{
			successlog();
			if(how.equalsIgnoreCase("uppercase"))
			{
				successlog();	
			}
			else
			{
				faillog("SEARCH BUTTON NAME IS NOT UPPERCASE IN THE WEB"+how);
				alltestcases.testcasesevena(extenttest, thisbrowser);
				takescreenshots.takepiconFail(driver, "TestcaseSeven");
				Assert.fail(" SEARCH BUTTON NAME IS NOT UPPERCASE IN THE WEB ");
			}
		}
		else
		{
			faillog("SEARCH BUTTON IS NOT ACTIVE IN THE WEB");
			alltestcases.testcaseseven(extenttest, thisbrowser);
			takescreenshots.takepiconFail(driver, "TestcaseSeven");
			Assert.fail(" SEARCH BUTTON IS NOT ACTIVE IN THE WEB ");
		}
		
	} 

}
