package tutsproject;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


public class takescreenshots {

	public static void takepiconFail(WebDriver driver, String fileName) throws FileNotFoundException {
		String path = "./Reports/" + fileName +"_"+settingDate()+ ".jpg";
		try {
			TakesScreenshot clickshot = (TakesScreenshot) driver;
			File source = clickshot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File(path));
			System.out.println(" - - - SCREENSHOT WERE TAKEN, CHECK /Reports - - - ");
		} catch (Exception e) {
			System.out.println("UNABLE TO TAKE SCREENSHOTS ");
		}

	}

	public static String settingDate() throws FileNotFoundException {
		//E yyyy.MM.dd 'at' hh:mm:ss a
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("EyyyyMMdd");
		String thisdate = ft.format(dNow);
		return thisdate;
	}

}
